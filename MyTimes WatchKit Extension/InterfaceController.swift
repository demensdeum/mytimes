//
//  InterfaceController.swift
//  MyTimes WatchKit Extension
//
//  Created by Ilia Prokhorov on 04/01/2019.
//  Copyright © 2019 demensdeum. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet private weak var table: WKInterfaceTable?
    private var dateFormatter = DateFormatter()
    private var dayOffset = 0
    private let days = 30
    private let rows: NSInteger
    
    override init() {
        rows = days + 2 // days + interface buttons + count
        super.init()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        dateFormatter.dateFormat = "dd MMM eee, yy"
        
        updateTable()
    }
    
    private func updateTable() {
        table?.setNumberOfRows(rows, withRowType: "dayRow")
        
        for controllerIndex in 0..<rows {
            if let controller = table?.rowController(at: controllerIndex) as? CalendarRowController {
                if controllerIndex == 0 {
                    controller.text = "<"
                }
                else if controllerIndex == rows - 1 {
                    controller.text = ">"
                }
                else {
                    controller.text = dateText(at: controllerIndex - 1)
                }
            }
        }
    }
    
    private func dateText(at index: NSInteger) -> String {
        let value = index + dayOffset
        guard let date = Calendar.current.date(byAdding: .day, value: value, to: Date()) else { return "-" }
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        if rowIndex == 0 {
            dayOffset -= 30
        }
        else if rowIndex == rows - 1 {
            dayOffset += 30
        }
        
        updateTable()
    }
}
