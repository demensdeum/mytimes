//
//  CalendarRowController.swift
//  MyTimes WatchKit Extension
//
//  Created by Ilia Prokhorov on 04/01/2019.
//  Copyright © 2019 demensdeum. All rights reserved.
//

import WatchKit

class CalendarRowController: NSObject {

    public var text = "-" {
        didSet {
            label?.setText(text)
        }
    }
    @IBOutlet private weak var label: WKInterfaceLabel?
    
}
